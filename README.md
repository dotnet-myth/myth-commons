<img  style="float: right;" src="Logo.jpg" alt="drawing" width="250"/>

# Myth Packages

[![pt-br](https://img.shields.io/badge/lang-pt--br-green.svg?style=for-the-badge)](/README.pt-br.md) [![en](https://img.shields.io/badge/lang-en-red.svg?style=for-the-badge)](/README.md)

[![License](https://img.shields.io/badge/License-Apache_2.0-blue.svg?style=for-the-badge)](https://opensource.org/licenses/Apache-2.0)

It is a set of simple and commonly reused libraries. They encompass all types of micro-functionality.

# ⭐ Packages

## 🔮 General features
- [Myth.Commons](Myth.Commons/README.md)

## 🛅 Manipulation of _assemblies_ and services
- [Myth.DependecyInjection](Myth.DependencyInjection/README.md)
- [Myth.DependecyInjection.Providers](Myth.DependencyInjection.Providers/README.md)

## 🎲 Access to databases
- [Myth.Specification](Myth.Specification/README.md)
- [Myth.Repository](Myth.Repository/README.md)
- [Myth.Repository.EntityFramework](Myth.Repository.EntityFramework/README.md)

## 🛜 HTTP Communication
- [Myth.Rest](Myth.Rest/README.md)