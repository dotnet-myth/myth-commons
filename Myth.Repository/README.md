# Myth.Repository

[![NuGet Version](https://img.shields.io/nuget/v/Myth.Repository?style=for-the-badge&logo=nuget)](https://www.nuget.org/packages/Myth.Repository/) [![NuGet Version](https://img.shields.io/nuget/vpre/Myth.Repository?style=for-the-badge&logo=nuget&color=rgb(255%2C%20185%2C%200))](https://www.nuget.org/packages/Myth.Repository/absoluteLatest)

[![License](https://img.shields.io/badge/License-Apache_2.0-blue.svg?style=for-the-badge)](https://opensource.org/licenses/Apache-2.0)

[![pt-br](https://img.shields.io/badge/lang-pt--br-green.svg?style=for-the-badge)](/README.pt-br.md) [![en](https://img.shields.io/badge/lang-en-red.svg?style=for-the-badge)](/README.md)

It is a .NET library for defining database access repositories.

# ⭐ Features
- Interfaces
- Setting for reading
- Definition for writing
- Definition for reading and writing
- Pagination
- Paginated search