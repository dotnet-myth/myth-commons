﻿namespace Myth.Exceptions;

public class InterfaceNotFoundException( string? message )
	: Exception( message ) {
}